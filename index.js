//JS - ES6 Updates
//1
let getCube = 3 ** 3;
console.log(`The cube of 3 is ${getCube}`);

//2 using ES6 Concat.
let address = [258, "Washingtong Ave NW", "California", 90011];
const [HouseNo, City, State, ZipCode] = address;
console.log(`I live at ${HouseNo} ${City} ${State} ${ZipCode}`);

//3 
const pet = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	unit: "kgs",
	measurementFeet: 20,
	measuremenInch: 3
}
const {name, type, weight, unit, measurementFeet, measuremenInch} = pet;
console.log(`${name} was a ${type}. He weighed at ${weight} ${unit} with a measurement of ${measurementFeet} ft ${measuremenInch} in.`)

//4 Array of numbers
// let numbers = [1, 2, 3, 4, 5];
// numbers.forEach(function(number) {
// 	console.log(number);
// });

let numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => {
	console.log(number);
});

//5 
class dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const myDog = new dog();
myDog.name = "Quebee";
myDog.age = 4;
myDog.breed = "Aspin"
console.log(myDog);